package routers

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/loopfz/gadgeto/tonic/utils/jujerr"
	"github.com/wI2L/fizz"
	"github.com/wI2L/fizz/openapi"
	"gitlab.com/kaeles/course-of-life-api/handlers"
	services "gitlab.com/kaeles/course-of-life-api/services"
)

// SetupRouter setup the application router
func SetupRouter() *gin.Engine {
	version := "1.0.0"
	tonic.SetErrorHook(jujerr.ErrHook)

	gin.SetMode(gin.ReleaseMode)

	engine := gin.Default()
	router := fizz.NewFromEngine(engine)
	router.Use(cors.Default())

	infos := &openapi.Info{
		Title:       "Course of Life",
		Description: "API for manage course of life",
		Version:     version,
	}
	router.GET("/openapi.json", nil, router.OpenAPI(infos, "json"))

	companyService := services.NewCompany()
	company := handlers.NewCompany(companyService)

	v1 := router.Group("/v1", "v1", "version 1 of routers")
	{
		// Company
		companyGroup := v1.Group("/companies", "companies", "companies routes")
		companyGroup.GET("",
			[]fizz.OperationOption{
				fizz.Summary("Find companies with options"),
				fizz.ID("findCompanies"),
			},
			tonic.Handler(company.FindCompanies, http.StatusOK),
		)
		companyGroup.GET("/:ID",
			[]fizz.OperationOption{
				fizz.Summary("Get company by its ID"),
				fizz.ID("getCompanyByID"),
			},
			tonic.Handler(company.GetCompanyByID, http.StatusOK),
		)
		companyGroup.POST("",
			[]fizz.OperationOption{
				fizz.Summary("Create company"),
				fizz.ID("CreateCompany"),
			},
			tonic.Handler(company.CreateCompany, http.StatusOK),
		)
		companyGroup.PATCH("/:ID",
			[]fizz.OperationOption{
				fizz.Summary("Update company"),
				fizz.ID("UpdateCompany"),
			},
			tonic.Handler(company.UpdateCompany, http.StatusOK),
		)
		companyGroup.DELETE("/:ID",
			[]fizz.OperationOption{
				fizz.Summary("Delete company"),
				fizz.ID("DeleteCompany"),
			},
			tonic.Handler(company.DeleteCompany, http.StatusOK),
		)
	}
	return engine
}
