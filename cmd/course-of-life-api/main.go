package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/kaeles/course-of-life-api/config"
	"gitlab.com/kaeles/course-of-life-api/routers"
)

func main() {
	logFormat, _ := os.LookupEnv("LOG_FORMAT")
	switch logFormat {
	case "HUMAN":
		log.Logger = log.Logger.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	case "JSON":
		// Already default
	default:
		log.Logger = log.Logger.Output(zerolog.ConsoleWriter{Out: os.Stderr, NoColor: true})
	}

	logLevel := os.Getenv("LOG_LEVEL")
	if strings.EqualFold(logLevel, "DEBUG") {
		log.Logger = log.Logger.Level(zerolog.DebugLevel)
		log.Debug().Msg("Logs are in Debug Mode")
	}

	parsedConfig, err := config.ParseConfig()
	if err != nil {
		log.Fatal().Err(err).Msg("unable to parse vault")
	}

	router := routers.SetupRouter()

	var port = parsedConfig.Server.Port

	log.Info().Msgf("Run on port %s", port)
	srv := &http.Server{
		Addr:              fmt.Sprintf(":%s", port),
		Handler:           router,
		ReadHeaderTimeout: time.Second * 5,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Fatal().Err(err).Msg("listen")
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-quit
	log.Info().Msg("Shutdown server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Error().Err(err).Msg("Server shutdown")
	}

	log.Info().Msg("server exiting")
}
