package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gitlab.com/kaeles/course-of-life-api/database"
	"gitlab.com/kaeles/course-of-life-api/services"
	"gitlab.com/kaeles/course-of-life-api/types"
)

// Company object to get all service functions
type Company struct {
	CompanyService *services.Company
}

// NewCompany constructor of Company
func NewCompany(services *services.Company) *Company {
	return &Company{
		CompanyService: services,
	}
}

// GetCompanyByID get a company by its ID
func (c Company) GetCompanyByID(ctx *gin.Context, input *types.CompanyInput) (*types.Company, error) {
	db, err := database.GetDB()
	if err != nil {
		log.Error().Err(err).Msg("")
		return nil, err
	}

	company, err := c.CompanyService.GetCompanyByID(input, db)
	if err != nil {
		log.Error().Err(err).Msg("")
		return nil, err
	}
	return company, nil
}

// FindCompanies find companies from criteria
func (c Company) FindCompanies(ctx *gin.Context, input *types.CompaniesInput) (*types.Companies, error) {
	db, err := database.GetDB()
	if err != nil {
		log.Error().Err(err).Msg("")
		return nil, err
	}

	companies, err := c.CompanyService.FindCompanies(input, db)
	if err != nil {
		log.Error().Err(err).Msg("")
		return nil, err
	}

	return companies, nil
}

// CreateCompany create a new company
func (c Company) CreateCompany(ctx *gin.Context, input *types.CreateCompanyInput) (*types.Company, error) {
	db, err := database.GetDB()
	if err != nil {
		log.Error().Err(err).Msg("")
		return nil, err
	}

	company, err := c.CompanyService.CreateCompany(input, db)
	if err != nil {
		log.Error().Err(err).Msg("")
		return nil, err
	}

	return company, nil
}

// UpdateCompany update a company
func (c Company) UpdateCompany(ctx *gin.Context, input *types.UpdateCompanyInput) (*types.Company, error) {
	db, err := database.GetDB()
	if err != nil {
		log.Error().Err(err).Msg("")
		return nil, err
	}

	company, err := c.CompanyService.UpdateCompany(input, db)
	if err != nil {
		log.Error().Err(err).Msg("")
		return nil, err
	}

	return company, err
}

// DeleteCompany delete a company
func (c Company) DeleteCompany(ctx *gin.Context, input *types.CompanyInput) error {
	db, err := database.GetDB()
	if err != nil {
		log.Error().Err(err).Msg("")
		return err
	}

	if err := c.CompanyService.DeleteCompany(input, db); err != nil {
		log.Error().Err(err).Msg("")
		return err
	}

	return nil
}
