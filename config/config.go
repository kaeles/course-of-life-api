package config

import (
	"fmt"
	"os"
	"sync"

	"github.com/go-playground/validator/v10"
	"gopkg.in/yaml.v3"
)

// APP the name of application
const APP = "course-of-life-api"

// validation use to validate the application config
var validation = validator.New()

// Config represents the application config
type Config struct {
	Server   Server   `yaml:"server" validate:"required"`
	Database Database `yaml:"database" validate:"required"`
	IsDev    bool
}

// Server contains the running port of the application
type Server struct {
	Port string `yaml:"port"`
}

// Database contains database config
type Database struct {
	Username string            `yaml:"user" validate:"required"`
	Password string            `yaml:"pass" validate:"required"`
	Host     string            `yaml:"host" validate:"required"`
	Port     string            `yaml:"port" validate:"required"`
	Options  map[string]string `yaml:"options"`
}

var config *Config
var once sync.Once

// ParseConfig parse config from vault
func ParseConfig() (*Config, error) {
	var err error
	once.Do(func() {
		config = &Config{}

		config.IsDev = os.Getenv("APP_ENV") == "dev"

		err = raiseFirstError(
			parse(config),
			validate(config),
		)
	})
	return config, err
}

func raiseFirstError(err ...error) error {
	for _, e := range err {
		if e != nil {
			return e
		}
	}

	return nil
}

func parse(config *Config) error {
	f, err := os.ReadFile("config.yml")
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(f, config)
	if err != nil {
		return err
	}

	return nil
}

func validate(st interface{}) error {
	if err := validation.Struct(st); err != nil {
		return fmt.Errorf("in key vault config: %w", err)
	}

	return nil
}
