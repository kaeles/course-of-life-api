OUT := course-of-life-api
PKG := gitlab.com/kaeles/course-of-life-api
VERSION := $(shell git describe --always --tags --dirty)
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/)
GO_LINT = ${GOPATH}/bin/golint
TARGET_DIR = ./dist
OUT_TARGET = $(TARGET_DIR)/${OUT}

$(GO_LINT):
	go get -u golang.org/x/lint/golint

install:
	go mod tidy

server: vet lint
	go build -o ${OUT_TARGET} -ldflags="-X main.version=${VERSION}" ${PKG}/cmd/${OUT}

serverDev: install lint
	go build -o ${OUT_TARGET} -ldflags="-X main.version=${VERSION}" ${PKG}/cmd/${OUT}

vet:
	@go vet ${PKG_LIST}

lint:
	@for file in ${GO_FILES} ; do \
		golint -set_exit_status $$file ; \
	done

static: lint
	bo build -v -o ${OUT_TARGET}-v${VERSION} -tags netgo -ldflags="-extldflags \"-static\" -w -s -X main.version=${VERSION}" ${PKG}/app

run: server
	ENV="" ${OUT_TARGET}

clean:
	-@rm ${OUT_TARGET} ${OUT_TARGET}-v*

APP_PORT ?= 8081

.PHONY: run server static vet lint install help