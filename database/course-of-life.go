package database

import (
	"fmt"
	"os"
	"sync"
	"time"

	"gitlab.com/kaeles/course-of-life-api/config"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

// DATABASE name of the database
const DATABASE = "course_of_life"

var pools = struct {
	sync.RWMutex
	pools map[string]*gorm.DB
}{
	pools: make(map[string]*gorm.DB),
}

// GetDB get gorm database instance
func GetDB() (*gorm.DB, error) {
	db, err := getConn()
	if err != nil {
		return nil, err
	}

	return db, nil
}

func connect(dsn string) (*gorm.DB, error) {
	config := &gorm.Config{
		NamingStrategy: schema.NamingStrategy{SingularTable: true},
	}

	if os.Getenv("SQL_DEBUG") != "" {
		config.Logger = logger.Default
		config.Logger.LogMode(logger.Info)
	}

	return gorm.Open(mysql.Open(dsn), config)

}

func getConn() (*gorm.DB, error) {
	var (
		conn    *gorm.DB
		ok      bool
		connErr error
	)

	cfg, err := config.ParseConfig()
	if err != nil {
		return nil, err
	}

	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		cfg.Database.Username,
		cfg.Database.Password,
		cfg.Database.Host,
		cfg.Database.Port,
		DATABASE,
	)

	pools.RLock()
	conn, ok = pools.pools[dsn]
	pools.RUnlock()

	if !ok {
		pools.Lock()
		defer pools.Unlock()
		conn, ok = pools.pools[dsn]
		if !ok {
			conn, connErr = connect(dsn)
			if connErr != nil {
				return nil, fmt.Errorf("error when connecting to database: %w", connErr)
			}
			sqlDB, err := conn.DB()
			if err != nil {
				return nil, fmt.Errorf("unable to access database: %w", err)
			}
			sqlDB.SetConnMaxLifetime(59 * time.Second)
			sqlDB.SetMaxIdleConns(10)
			sqlDB.SetMaxOpenConns(100)
			pools.pools[dsn] = conn
		}
	}

	sqlDB, err := conn.DB()
	if err != nil {
		return nil, fmt.Errorf("unable to access database: %w", err)
	}

	if tryErr := sqlDB.Ping(); tryErr != nil {
		return nil, tryErr
	}

	return conn, nil
}
