package services

import (
	"database/sql"

	"errors"

	"github.com/go-playground/validator/v10"
	jujuErrors "github.com/juju/errors"

	"gitlab.com/kaeles/course-of-life-api/types"
	"gorm.io/gorm"
)

// Company object to get service validator
type Company struct {
	validate *validator.Validate
}

// NewCompany constructor of Company
func NewCompany() *Company {
	return &Company{
		validate: validator.New(),
	}
}

// GetCompanyByID get a company by its ID
func (c Company) GetCompanyByID(input *types.CompanyInput, db *gorm.DB) (*types.Company, error) {
	company := types.Company{ID: input.ID}
	err := db.First(&company).Error
	if errors.Is(err, gorm.ErrRecordNotFound) || errors.Is(err, sql.ErrNoRows) {
		err = jujuErrors.NewNotFound(err, "Company not found")
	}
	if err != nil {
		return nil, err
	}
	return &company, nil
}

// FindCompanies find companies from criteria
func (c Company) FindCompanies(input *types.CompaniesInput, db *gorm.DB) (*types.Companies, error) {
	companies := types.Companies{}

	count, err := c.Count(input, db)
	if err != nil {
		return nil, err
	}
	companies.Total = count.Total
	companies.TotalFiltered = count.TotalFiltered

	db = c.setWhereFromInput(input, db)

	err = db.Find(&companies.Items).Error
	if err != nil {
		return nil, err
	}

	return &companies, nil
}

func (c Company) setWhereFromInput(input *types.CompaniesInput, db *gorm.DB) *gorm.DB {
	db = db.Session(&gorm.Session{NewDB: true})

	db = db.Where(input)

	if len(input.IDs) > 0 {
		db = db.Where("ids IN (?)", input.IDs)
	}

	if input.Limit != 0 {
		db = db.Limit(input.Limit)
	}

	if input.Offset != 0 {
		db = db.Offset(input.Offset)
	}

	return db
}

// Count count companies
func (c Company) Count(input *types.CompaniesInput, db *gorm.DB) (*types.CompaniesCount, error) {
	count := types.CompaniesCount{}

	err := db.Model(&types.Company{}).Count(&count.Total).Error
	if err != nil {
		return nil, err
	}

	db = c.setWhereFromInput(input, db)
	err = db.Model(&types.Company{}).Count(&count.TotalFiltered).Error
	if err != nil {
		return nil, err
	}

	return &count, nil
}

// CreateCompany create a new company
func (c Company) CreateCompany(input *types.CreateCompanyInput, db *gorm.DB) (*types.Company, error) {
	if err := c.validate.Struct(input); err != nil {
		return nil, err
	}

	company := types.Company{}
	if err := db.Create(&input).Error; err != nil {
		return nil, err
	}
	if err := db.First(&company, input.ID).Error; err != nil {
		return nil, err
	}

	return &company, nil
}

// UpdateCompany update a company
func (c Company) UpdateCompany(input *types.UpdateCompanyInput, db *gorm.DB) (*types.Company, error) {
	if err := c.validate.Struct(input); err != nil {
		return nil, err
	}

	company := types.Company{}
	if err := db.Updates(&input).Error; err != nil {
		return nil, err
	}
	if err := db.First(&company, input.ID).Error; err != nil {
		return nil, err
	}

	return &company, nil
}

// DeleteCompany delete a company
func (c Company) DeleteCompany(input *types.CompanyInput, db *gorm.DB) error {
	return db.Delete(&types.Company{ID: input.ID}).Error
}
