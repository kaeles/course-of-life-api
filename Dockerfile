FROM golang:1.20

RUN mkdir -p /app

WORKDIR /app

COPY ./ /app/course-of-life-api

RUN chown -R nobody:nogroup /app && chmod +x /app/course-of-life-api
USER nobody

EXPOSE 8081

CMD [ "/app/course_of_life" ]
