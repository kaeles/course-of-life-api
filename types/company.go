package types

import "time"

var companyTableName = "company"

// Company represents a company
type Company struct {
	ID          int       `json:"ID" gorm:"column:company_id"`
	Name        string    `json:"name" gorm:"column:name"`
	StartDate   time.Time `json:"startDate" gorm:"column:start_date"`
	EndDate     time.Time `json:"endDate" gorm:"column:end_date"`
	Description string    `json:"description" gorm:"column:description"`

	CreationDate time.Time `json:"creationDate" gorm:"column:creation_date"`
	CreationUser string    `json:"creationUser" gorm:"column:creation_user"`
	UpdateDate   time.Time `json:"updateDate" gorm:"column:update_date"`
	UpdateUser   time.Time `json:"updateUser" gorm:"column:update_user"`
}

// TableName defines the name of table in DB
func (*Company) TableName() string {
	return companyTableName
}

// Companies contains a list of Company and its count
type Companies struct {
	Items []Company `json:"items" gorm:"-"`
	CompaniesCount
}

// TableName defines the name of table in DB
func (*Companies) TableName() string {
	return companyTableName
}

// CompaniesCount total and total filtered companies.
type CompaniesCount struct {
	Total         int64 `json:"total"`
	TotalFiltered int64 `json:"totalFiltered"`
}

// TableName defines the name of table in DB
func (*CompaniesCount) TableName() string {
	return companyTableName
}

// CompanyInput input to get or delete company
type CompanyInput struct {
	ID int `path:"ID" validate:"required"`
}

// TableName defines the name of table in DB
func (*CompanyInput) TableName() string {
	return companyTableName
}

// CompaniesInput input to find companies from criteria
type CompaniesInput struct {
	ID          int       `query:"ID" gorm:"column:company_id"`
	IDs         []int     `query:"IDs" gorm:"-"`
	Name        string    `query:"name" gorm:"column:name"`
	StartDate   time.Time `query:"startDate" gorm:"column:start_date"`
	EndDate     time.Time `query:"endDate" gorm:"column:end_date"`
	Description string    `query:"description" gorm:"column:description"`

	Limit  int `query:"limit" gorm:"-"`
	Offset int `query:"offset" gorm:"-"`
}

// TableName defines the name of table in DB
func (*CompaniesInput) TableName() string {
	return companyTableName
}

// CreateCompanyInput input to create a company
type CreateCompanyInput struct {
	ID          int        `json:"ID" gorm:"column:company_id"`
	Name        string     `body:"name" gorm:"column:name" validate:"required"`
	StartDate   time.Time  `body:"startDate" gorm:"column:start_date" validate:"required"`
	EndDate     *time.Time `body:"endDate" gorm:"column:end_date"`
	Description *string    `body:"description" gorm:"column:description"`

	CreationDate *time.Time `json:"creationDate" gorm:"column:creation_date"`
	CreationUser *string    `json:"creationUser" gorm:"column:creation_user"`
}

// TableName defines the name of table in DB
func (*CreateCompanyInput) TableName() string {
	return companyTableName
}

// UpdateCompanyInput input to update a company
type UpdateCompanyInput struct {
	ID          int        `path:"ID" gorm:"column:company_id" validate:"required"`
	Name        *string    `body:"name" gorm:"column:name"`
	StartDate   *time.Time `body:"startDate" gorm:"column:start_date"`
	EndDate     *time.Time `body:"endDate" gorm:"column:end_date"`
	Description *string    `body:"description" gorm:"column:description"`

	CreationDate *time.Time `json:"creationDate" gorm:"column:creation_date"`
	CreationUser *string    `json:"creationUser" gorm:"column:creation_user"`
	UpdateDate   *time.Time `json:"updateDate" gorm:"column:update_date"`
	UpdateUser   *time.Time `json:"updateUser" gorm:"column:update_user"`
}

// TableName defines the name of table in DB
func (*UpdateCompanyInput) TableName() string {
	return companyTableName
}
